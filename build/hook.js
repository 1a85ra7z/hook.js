(() => {
    let sdi = [];
    let sdt = [];

    let s = {};
    let j = [];

    let cv = localStorage.getItem('-hook-version--data') !== null ? localStorage.getItem('-hook-version--data') : '-1';
    let hfr = true;

    let hurl = {};

    window.location.search.substr(1).split('&').forEach(qp => {
        hurl[qp.split('=')[0]] = decodeURIComponent(qp.split('=')[1]).toString();
    });

    let rn = () => {
        [
            ...document.getElementsByTagName('script'),
            ...document.getElementsByTagName('link'),
            ...document.getElementsByTagName('img')
        ].forEach(e => {
            if(e.getAttribute('hook') !== null) {
                let set = e.getAttribute('hook').split('::')[0];
                let to = e.getAttribute('hook').split('::')[1];

                hook.get(to, (data, object) => {
                    if(object.t === 'string') {
                        if(e.tagName.toLowerCase() === 'script' && e.getAttribute('private')) {
                            hook.run(to, () => {});
                        } else {
                            let d = document.createElement(e.tagName === 'LINK' ? 'STYLE' : e.tagName);
                            d.classList.add(`hook::${to}`);
                            document.body.appendChild(d);

                            d.innerHTML = data;

                            e.remove();
                        }
                    } else if(object.t === 'image' || object.t === 'vector') {
                        let fd;

                        if(object.t === 'image') {
                            let bc = window.atob(data);
                            let bn = new Array(bc.length);

                            for(let i = 0; i < bc.length; i++) {
                                bn[i] = bc.charCodeAt(i);
                            }

                            fd = window.URL.createObjectURL(new Blob([new Uint8Array(bn)]))
                        } else {
                            fd = window.URL.createObjectURL(new Blob([data], {type: 'image/svg+xml'}))
                        }

                        e.setAttribute(set, data === '' ? '' : fd);
                        e.classList.add(`hook::${to}`);
                    }
                });
            }
        });
    };

    let rhcac = frd => {
        if (
            cv !== (frd.split(':').length === 2 ? frd.split(':')[0] : '-1')
        )  {
            g(
                (
                    document.querySelector('hook') !== undefined ?
                        (
                            document.querySelector('hook').getAttribute('src') !== undefined ?
                                document.querySelector('hook').getAttribute('src') : './data.hook'
                        ) : './data.hook'
                ), srd => {
                    if (hfr) {
                        localStorage.setItem('-hook--data', srd);

                        s = JSON.parse(localStorage.getItem('-hook--data'));

                        s.hook.forEach(d => {
                            hook.resolve(d.name, (x) => {
                                j.push(x);

                                if (s.hook.length === j.length) {
                                    rn();
                                }
                            });
                        });

                        hfr = false;
                    } else {
                        JSON.parse(srd).hook.forEach((ud, ui) => {
                            if (ud.version !== s.hook[ui].version) {
                                hook.update(ud);
                            }
                        });

                        if(hook.dev) {
                            hook.reload();
                        }
                    }

                    localStorage.setItem('-hook-version--data', (frd.split(':').length === 2 ? frd.split(':')[0] : -1));

                    cv = frd.split(':').length === 2 ? frd.split(':')[0] : '-1';
                }
            );
        } else if(hfr) {
            s = JSON.parse(localStorage.getItem('-hook--data'));

            s.hook.forEach(d => {
                hook.resolve(d.name, (x) => {
                    j.push(x);

                    if (s.hook.length === j.length) {
                        rn();
                    }
                });
            });

            hfr = false;
        }
    };

    let rhca = () => {
        if(localStorage.getItem('-hook--version') === null ? false : (localStorage.getItem('-hook--version').length > 0)) {
            rhcac(localStorage.getItem('-hook--version'));
            localStorage.setItem('-hook--version', '');
        } else {
            g((
                document.querySelector('hook') !== null ?
                    (
                        document.querySelector('hook').getAttribute('version') !== undefined ?
                            document.querySelector('hook').getAttribute('version') : './version.hook'
                    ) : './version.hook'
            ), frd => {
                rhcac(frd);
            });
        }
    };

    setInterval(() => {
        rhca();
    }, 15000);

    window.hook = {
        listen: (element, event, callback, renderingCondition, once) => {
            let di = true;

            if(element !== undefined) {
                if(typeof renderingCondition === 'object' ? (renderingCondition.filter(v => { return rlo.includes(v) }).length > 0) : true) {
                    let cbf = (ce) => {
                        if(di) {
                            callback(ce);

                            if(once) {
                                di = false;
                            }
                        } else {
                            element.removeEventListener(event, cbf);
                        }
                    };

                    element.addEventListener(event, cbf);
                }
            }

            return element === undefined;
        },

        clearIntervals: () => {
            sdi.forEach(ci => {
                clearInterval(ci);
            });
        },

        clearTimeouts: () => {
            sdt.forEach(ci => {
                clearTimeout(ci);
            });
        },

        interval: (callback, timeout) => {
            sdi.push(
                setInterval(callback, timeout)
            )
        },

        timeout: (callback, timeout) => {
            sdt.push(
                setTimeout(callback, timeout)
            );
        },

        setup: rn,

        clear: () => {
            localStorage.clear();
        },

        reload: () => {
            let urls = '/';

            Object.keys(hook.url.parameters).forEach((urlk, urli) => {
                if(typeof hook.url.parameters[urlk] === 'string' ? hook.url.parameters[urlk].length > 0 : false) {
                    if(urli > 0) {
                        urls += '&';
                    } else {
                        urls += '?';
                    }

                    urls += `${urlk}=${hook.url.parameters[urlk]}`;
                }
            });

            window.history.replaceState({}, document.title, urls);
            window.location.reload();
        },

        reset: () => {
            hook.removeAll();
            hook.clear();
            hook.url.clear();
            hook.reload();
        },

        load: (d, c) => {
            let q = new XMLHttpRequest();

            q.addEventListener('readystatechange', () => {
                if(q.status === 200 && q.readyState === 4) {
                    if(/\S*\s*\.png|\.jpg/g.test(s.hook[d.f].path)) {
                        let re = new FileReader();

                        re.addEventListener('loadend', () => {
                            let x = {
                                d: re.result.replace(/^data:.+;base64,/, ''),
                                n: d.n,
                                v: s.hook[d.f].version,
                                t: 'image',
                                p: s.hook[d.f].path
                            };

                            localStorage.setItem(d.n, JSON.stringify(x));
                            typeof c === 'function' ? c(x) : null;
                        });

                        re.readAsDataURL(q.response);
                    } else if(/\S*\s*\.svg/g.test(s.hook[d.f].path)) {
                        let x = {
                            d: q.responseText,
                            n: d.n,
                            v: s.hook[d.f].version,
                            t: 'vector',
                            p: s.hook[d.f].path
                        };

                        localStorage.setItem(d.n, JSON.stringify(x));
                        typeof c === 'function' ? c(x) : null;
                    } else {
                        let x = {
                            d: q.responseText,
                            n: d.n,
                            v: s.hook[d.f].version,
                            t: 'string',
                            p: s.hook[d.f].path
                        };

                        localStorage.setItem(d.n, JSON.stringify(x));
                        typeof c === 'function' ? c(x) : null;
                    }
                }
            });

            if(/\S*\s*\.png|\.jpg/g.test(s.hook[d.f].path)) {
                q.responseType = 'blob';
            }

            q.open('GET', s.hook[d.f].path);
            q.send();
        },

        remove: s => {
            if(document.getElementsByClassName(`hook::${s}`).length > 0) {
                document.getElementsByClassName(`hook::${s}`)[0].remove();

                return true;
            }

            return false;
        },

        removeAll: () => {
            j.forEach(ce => {
                hook.remove(ce.n);

                localStorage.removeItem(ce.n);
            });
        },

        run: (s, c, p) => {
            hook.get(s, (d, o) => {
                if(document.getElementsByClassName(`hook::${s}`).length > 0) {
                    hook.remove(s);
                }

                let e = document.createElement('script');

                o.callback = c;

                e.innerHTML = `
                    (() => {let __componentId = '${s}';let __parameters = JSON.parse(\`${typeof p === 'object' ? JSON.stringify(p).replace(/\\/g, '\\\\') : '{}'}\`);let done = (...args) => { ${typeof c === 'function' ? `hook.get('${s}', (d, o) => {o.callback(args);})` : null }};)();
                `;

                e.classList.add(`hook::${s}`);
                document.body.appendChild(e);
            });
        },

        get: (f, c) => {
            j.forEach(jj => {
                if(jj.n === f) {
                    typeof c === 'function' ? c(jj.d, jj) : null;
                }
            });
        },

        resolve: (a, c) => {
            let f = -1;

            s.hook.forEach((h, i) => {
                if(h.name === a) {
                    f = i;
                }
            });

            if(f > -1) {
                let cd = window.localStorage.getItem(a);

                if (cd !== null && cd !== undefined) {
                    cd = JSON.parse(cd);

                    if(cd.v === s.hook[f].version) {
                        c(cd);
                    } else {
                        hook.load({
                            f: f,
                            n: a
                        }, x => {
                            typeof c === 'function' ? c(x) : null;
                        });
                    }
                } else {
                    hook.load({
                        f: f,
                        n: a
                    }, x => {
                        typeof c === 'function' ? c(x) : null;
                    });
                }
            }
        },

        update: c => {
            s.hook.forEach((ce, i) => {
                if(ce.name === c.name) {
                    hook.load({f: i, n: c.name}, (x) => {
                        j[i] = x;
                    });
                }
            });
        },

        url: {
            parameters: hurl,
            clear: () => {
                hook.url.parameters = {};
            }
        }
    };

    rhca();
})();