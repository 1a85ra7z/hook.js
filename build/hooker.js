'use strict';
let wl = false; window.addEventListener('load', () => { wl = true; });
let h = () => {
    let s = document.createElement('script');
    s.innerHTML = localStorage.getItem('-h') !== null ? (localStorage.getItem('-h').length !== 0 ? localStorage.getItem('-h') : '() => {}') : '() => {}';
    let i = setInterval(() => {
        if(wl) {
            clearInterval(i);
            document.body.append(s);
        }
    })
};
let g = (f, c) => {
    let r = new XMLHttpRequest();
    r.addEventListener('readystatechange', () => {
        if(r.readyState === 4 && r.status === 200) {
            c(r.responseText);
        }
    });
    r.open('GET', f);
    r.send();
};
g('./version.hook', (d) => {
    if(localStorage.getItem('-h-v') !== null ? d !== localStorage.getItem('-h-v') : true) {
        g('./hook.min.js', (hc) => {
            localStorage.setItem('-h', hc);
            localStorage.setItem('-h--v', d);
            h();
        });
    } else {
        h();
    }
});